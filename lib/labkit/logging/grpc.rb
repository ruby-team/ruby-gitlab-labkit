# frozen_string_literal: true

module Labkit
  module Logging
    module GRPC
      autoload :ServerInterceptor, "labkit/logging/grpc/server_interceptor"
    end
  end
end
